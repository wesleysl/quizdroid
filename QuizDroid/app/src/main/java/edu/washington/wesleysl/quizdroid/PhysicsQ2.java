package edu.washington.wesleysl.quizdroid;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class PhysicsQ2 extends ActionBarActivity {
    private RadioGroup radioSexGroup;
    private RadioButton radioSexButton;
    private Button btnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_physics_q1);
        // Locate the button in activity_main.xml
        btnNext = (Button) findViewById(R.id.physbutton);

        // Capture button clicks
        btnNext.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {

                // Start NewActivity.class
                Intent myIntent = new Intent(PhysicsQ2.this,
                        PhysicsOverview.class);
                startActivity(myIntent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_physics_q1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addListenerOnButton() {

        radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);
        btnNext = (Button) findViewById(R.id.buttonq2);

        btnNext.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
            //sint selectedId = radioSexGroup.getCheckedRadioButtonId();

            //radioSexButton = (RadioButton) findViewById(selectedId);

            Intent myIntent = new Intent(PhysicsQ2.this,
                PhysicsOverview.class);
            startActivity(myIntent);
            }

        });

    }
}